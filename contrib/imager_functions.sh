get_metadata_value() {
	awk '/^Metadata:$/,EOF' "$1" | grep "^$2:" | cut -d ' ' -f '2-'
}

generate_imager_sublist() {
	cat <<EOF >"$1"
{
  "os_list": [
    {
      "name": "$IMAGER_NAME",
      "description": "$IMAGER_DESCRIPTION",
      "url": "$IMAGER_URL",
      "icon": "$IMAGER_ICON",
      "website": "$IMAGER_WEBSITE",
      "release_date": "$IMAGER_RELEASE_DATE",
      "extract_size": $IMAGER_EXTRACT_SIZE,
      "extract_sha256": "$IMAGER_EXTRACT_SHA256",
      "image_download_size": $IMAGER_IMAGE_DOWNLOAD_SIZE,
      "image_download_sha256": "$IMAGER_IMAGE_DOWNLOAD_SHA256",
      "devices": $IMAGER_DEVICES,
      "init_format": "$IMAGER_INIT_FORMAT"
    }
  ]
}
EOF
}

generate_imager_list() {
	echo "Generate imager list..." >&2

	repo_url="$1"
	echo "Download $repo_url" >&2
	if curl -fsL "$repo_url"; then
		cat <<EOF
{
  "os_list": [
    {
      "name": "$IMAGER_NAME",
      "description": "$IMAGER_DESCRIPTION",
      "icon": "$IMAGER_ICON",
      "website": "$IMAGER_WEBSITE",
      "subitems_url": "$IMAGER_SUBITEMS_URL"
    }
  ]
}
EOF
	else
		echo "Download failed." >&2
	fi | jq -s 'add'
	echo "Done." >&2
}

merge_imager_sublists() {
	echo "Merge imager sublists..." >&2

	base_url="$1"
	shift
	for json_file in "$@"; do
		if [ -f "$json_file" ]; then
			echo "Found $json_file" >&2
			cat "$json_file"
		else
			echo "Download $base_url/$json_file" >&2
			if curl -fsLo "$json_file" "$base_url/$json_file"; then
				cat "$json_file"
			else
				echo "Download failed." >&2
			fi
		fi
	done | jq -s '{os_list: [.[] | .os_list[]]}'
	echo "Done." >&2
}
