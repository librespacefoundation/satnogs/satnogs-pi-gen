#!/usr/bin/env python3
#
# Script to render index HTML file from template

import json
import os
from pathlib import Path

from jinja2 import Environment, FileSystemLoader

TEMPLATE_PATH = 'contrib/templates/index.html.j2'
IMAGER_JSON_PATH = 'public/rpi-imager.json'
OUTPUT_PATH = 'public/index.html'


def load_imager_json(path):
    """Load RPi imager JSON file

    :param path: Path to JSON file
    :type path: Path
    :return: Dictionary of parsed JSON
    :rtype: dict
    """
    with open(Path(path), mode='r', encoding='utf-8') as file:
        data = json.load(file)

    return data


def render_page(template_path, output_path):
    """Renders the selected template and writes the render output to the
    specified file.

    :param templates_path: Path to the Jinja2 template file
    :param output_path: Path to the output file
    """
    file_loader = FileSystemLoader(Path(template_path).parent)
    env = Environment(loader=file_loader, trim_blocks=True, lstrip_blocks=True)

    index_template = env.get_template(Path(template_path).name)
    index_template.stream(
        env=os.environ,
        imager=load_imager_json(IMAGER_JSON_PATH)).dump(output_path)


if __name__ == '__main__':
    render_page(TEMPLATE_PATH, OUTPUT_PATH)
