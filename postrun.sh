#!/bin/sh -e

printf "\nMetadata:\n" >> "${DEPLOY_DIR}/${IMG_FILENAME}${IMG_SUFFIX}.info"
cat <<EOF >> "${DEPLOY_DIR}/${IMG_FILENAME}${IMG_SUFFIX}.info"
archive_filename: ${ARCHIVE_FILENAME}${IMG_SUFFIX}.img.xz
release_date: $IMG_DATE
extract_size: $(stat --printf "%s\n" "${STAGE_WORK_DIR}/${IMG_FILENAME}${IMG_SUFFIX}.img")
extract_sha256: $(sha256sum -b "${STAGE_WORK_DIR}/${IMG_FILENAME}${IMG_SUFFIX}.img" | cut -d " " -f 1 -)
image_download_size: $(stat --printf "%s\n" "${DEPLOY_DIR}/${ARCHIVE_FILENAME}${IMG_SUFFIX}.img.xz")
image_download_sha256: $(sha256sum -b "${DEPLOY_DIR}/${ARCHIVE_FILENAME}${IMG_SUFFIX}.img.xz" | cut -d " " -f 1 -)
EOF
