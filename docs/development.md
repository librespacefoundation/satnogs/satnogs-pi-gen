# Development Documentation


## Branching

- Single branch (`master`)
- Merge upstream tagged releases only

## Tagging

- Annotated tags
- Tag follows date format: `%Y%m%d<serial number>`
- Serial number starts from `00` and is reset on any date change


# Release Steps (minimal guide)

1. Tag version
   ```
   TAG=2023111400
   git tag -m "Tag version '$TAG'" $TAG
   ```

2. Push tag to upstream repo
   ```
   git push upstream $TAG
   ```

3. Wait for CI to finish successfully. Test.

4. Create a [new](https://gitlab.com/librespacefoundation/satnogs/satnogs-pi-gen/-/releases/new) release in gitlab. Use the following script to generate the release notes there: 
   ```bash
   #!/usr/bin/bash

   TAG=2022091000
   DATE=2022-09-10

   cat <<EOF
   [![pipeline status](https://gitlab.com/librespacefoundation/satnogs/satnogs-pi-gen/badges/$TAG/pipeline.svg)](https://gitlab.com/librespacefoundation/satnogs/satnogs-pi-gen/commits/$TAG)
   * Downloads
     * [Zipped image](https://gitlab.com/librespacefoundation/satnogs/satnogs-pi-gen/-/jobs/artifacts/$TAG/raw/deploy/image_$DATE-Raspbian-SatNOGS-lite.zip?job=release)
     * [Image info](https://gitlab.com/librespacefoundation/satnogs/satnogs-pi-gen/-/jobs/artifacts/$TAG/raw/deploy/$DATE-Raspbian-SatNOGS-lite.info?job=release)
     * [SHA256 checksums](https://gitlab.com/librespacefoundation/satnogs/satnogs-pi-gen/-/jobs/artifacts/$TAG/raw/deploy/sha256sums?job=release)
   EOF
   ```
5. Update the wiki in section [Raspberry Pi#Download](https://wiki.satnogs.org/Raspberry_Pi#Download)

6. Write a release post in community in the [Station Software Upgrades Announcement Thread](https://community.libre.space/t/announcement-satnogs-station-software-upgrades-are-available/7994/1)
