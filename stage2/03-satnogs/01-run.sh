#!/bin/bash -e

curl -sLf -o ${ROOTFS_DIR}/usr/local/bin/satnogs-setup https://satno.gs/install
chmod 755 ${ROOTFS_DIR}/usr/local/bin/satnogs-setup
